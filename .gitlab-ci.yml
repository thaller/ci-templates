
# We can not do multi-level includes, so we need to manually include all of
# our templates here
include:
  # projects using these templates should not need to pull the bootstrap
  - local: '/bootstrap/bootstrap.yml'

  # Alpine container builder template
  # projects using this should reference this with the following:
  #
  # - project: 'freedesktop/ci-templates'
  #   ref: master # or git sha, see https://docs.gitlab.com/ce/ci/yaml/#includefile
  #   file: '/templates/alpine.yml'
  #
  - local: '/templates/alpine.yml'
  - local: '/.gitlab-ci/alpine-ci.yml'

  # Arch container builder template
  # projects using this should reference this with the following:
  #
  # - project: 'freedesktop/ci-templates'
  #   ref: master # or git sha, see https://docs.gitlab.com/ce/ci/yaml/#includefile
  #   file: '/templates/arch.yml'
  #
  - local: '/templates/arch.yml'
  - local: '/.gitlab-ci/arch-ci.yml'

  # Centos container builder template
  # projects using this should reference this with the following:
  #
  # - project: 'freedesktop/ci-templates'
  #   ref: master # or git sha, see https://docs.gitlab.com/ce/ci/yaml/#includefile
  #   file: '/templates/centos.yml'
  #
  - local: '/templates/centos.yml'
  - local: '/.gitlab-ci/centos-ci.yml'

  # Debian container builder template
  # projects using this should reference this with the following:
  #
  # - project: 'freedesktop/ci-templates'
  #   ref: master # or git sha, see https://docs.gitlab.com/ce/ci/yaml/#includefile
  #   file: '/templates/debian.yml'
  #
  - local: '/templates/debian.yml'
  - local: '/.gitlab-ci/debian-ci.yml'

  # Fedora container builder template
  # projects using this should reference this with the following:
  #
  # - project: 'freedesktop/ci-templates'
  #   ref: master # or git sha, see https://docs.gitlab.com/ce/ci/yaml/#includefile
  #   file: '/templates/fedora.yml'
  #
  - local: '/templates/fedora.yml'
  - local: '/.gitlab-ci/fedora-ci.yml'

  # Ubuntu container builder template
  # projects using this should reference this with the following:
  #
  # - project: 'freedesktop/ci-templates'
  #   ref: master # or git sha, see https://docs.gitlab.com/ce/ci/yaml/#includefile
  #   file: '/templates/ubuntu.yml'
  #
  - local: '/templates/ubuntu.yml'
  - local: '/.gitlab-ci/ubuntu-ci.yml'


stages:
  - sanity check
  - bootstrapping
  - bootstrapping_qemu
  - alpine_container_build
  - alpine_check
  - arch_container_build
  - arch_check
  - centos_container_build
  - centos_check
  - debian_container_build
  - debian_check
  - fedora_container_build
  - fedora_check
  - ubuntu_container_build
  - ubuntu_check
  - deploy
  - publish
  - test published images


#
# We want those to fail as early as possible, so we are using a plain fedora
# image, and there is no need to run `dnf update` as we only need to run
# one python script.
#

.pip_install:
  stage: sanity check
  image: python:alpine
  before_script:
    - apk add git

sanity check:
  extends: .pip_install
  script:
    - pip3 install --user jinja2 PyYAML
    - python3 ./src/generate_templates.py

    - git diff --exit-code && exit 0 || true

    - echo "some files were not generated through 'src/generate_templates.py' or
      have not been committed. Please edit the files under 'src', run
      'src/generate_templates.py' and then commit the result"
    - exit 1


check commits:
  extends: .pip_install
  script:
    - pip3 install .
    - ci-fairy check-commits --signed-off-by --junit-xml=results.xml
  except:
    - master@freedesktop/ci-templates
  variables:
    GIT_DEPTH: 100
    GIT_STRATEGY: clone
  artifacts:
    reports:
      junit: results.xml


pytest ci-fairy:
  extends: .pip_install
  script:
    - pip3 install pytest
    - pip3 install .
    - pytest --junitxml=results.xml
  artifacts:
    reports:
      junit: results.xml


flake8 ci-fairy:
  extends: .pip_install
  script:
    - pip3 install flake8
    # 501: line too long
    # 504: line break after binary operator
    # 741: ambiguous variable name
    - flake8 --ignore=W501,E501,W504,W741,E741


check merge request:
  extends: .pip_install
  stage: deploy
  script:
    - pip3 install .
    - ci-fairy check-merge-request --require-allow-collaboration --junit-xml=check-merge-request.xml
  artifacts:
    expire_in: 1 week
    when: on_failure
    paths:
      - check-merge-request.xml
    reports:
      junit: check-merge-request.xml
  variables:
    FDO_UPSTREAM_REPO: freedesktop/ci-templates
  # We allow this to fail because no MR may have been filed yet
  allow_failure: true


pages:
  extends: .pip_install
  stage: deploy
  script:
  - pip3 install sphinx sphinx-rtd-theme
  # Upstream bug in the HTML rendering for YAML nodes so let's used the fixed version
  # until this gets merged
  - pip3 install git+https://github.com/whot/sphinxcontrib-autoyaml.git@wip/fix-definition-rendering
  - sh -x doc/build-docs.sh
  - mv build public
  artifacts:
    paths:
    - public

bootstrap@x86_64:
  extends: .bootstrap@x86_64


bootstrap@aarch64:
  extends: .bootstrap@aarch64


bootstrap-qemu@x86_64:
  extends: .qemu@x86_64
  needs:
    - bootstrap@x86_64


bootstrap-qemu-mkosi@x86_64:
  extends: .qemu-mkosi@x86_64
  needs:
    - bootstrap@x86_64

#
# Verify that all quay.io images directly referenced by our templates
# exist
#
test published images:
  image: $CI_REGISTRY_IMAGE/x86_64/buildah:2020-10-30.1
  stage: test published images
  script:
    - skopeo inspect docker://quay.io/freedesktop.org/ci-templates:buildah-aarch64-2020-10-30.1
    - skopeo inspect docker://quay.io/freedesktop.org/ci-templates:buildah-x86_64-2020-10-30.1
    - skopeo inspect docker://quay.io/freedesktop.org/ci-templates:qemu-base-x86_64-2020-10-30.1
    - skopeo inspect docker://quay.io/freedesktop.org/ci-templates:qemu-mkosi-base-x86_64-2020-10-30.1
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PROJECT_PATH == "freedesktop/ci-templates"'