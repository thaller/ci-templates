#!/bin/bash

usage() {
    echo "Usage: vmctl {start|start-kernel|stop|exec} [command]"
    echo ""
    echo "Options: "
    echo "  start ... start the virtual machine"
    echo "  start-kernel ... start the virtual machine with a custom kernel"
    echo "  stop  ... stop the virtual machine"
    echo "  exec  ... exec command on the virtual machine"
}

do_start() {
    set -ex

    if [[ ! -e /app/image.raw ]]
    then
      xz -d -T0 /app/image.raw.xz || (echo "Failed to unpack image" && exit 1)
    fi

    qemu-system-x86_64 -machine accel=kvm \
                       -smp 2 -m 1024 \
                       -drive format=raw,file=/app/image.raw \
                       -device virtio-net-pci,netdev=net0 \
                       -netdev user,id=net0,hostfwd=tcp::5555-:22 \
                       -display none \
                       -daemonize \
                       "$@" \
                       -serial file:$CI_BUILDS_DIR/$CI_PROJECT_PATH/console.out

    exit_code=$?

    if [[ ! -e /etc/ssh/ssh_config.d/99-vm.conf ]]; then
        cat >/etc/ssh/ssh_config.d/99-vm.conf <<EOF
            Host vm
              HostName localhost
              Port 5555
EOF
    fi

    # Connect once to store the host key locally
    if [[ $exit_code -eq 0 ]]; then
        ssh -o StrictHostKeyChecking=accept-new -o PubkeyAcceptedKeyTypes=ssh-rsa vm uname -a
        exit_code=$?
    fi

    if [[ $exit_code -ne 0 ]]; then
       echo "***********************************************************"
       echo "*                                                         *"
       echo "*       WARNING: failed to start or connect to VM         *"
       echo "*                                                         *"
       echo "***********************************************************"
    fi

    exit $exit_code
}

do_start_kernel() {
    set -x

    KERNEL=$1
    shift

    set -e

    # default to the latest kernel
    if [[ x"$KERNEL" == x"" ]]
    then
        KERNEL=$(ls /app/vmlinuz* | sort | tail -1)
    fi

    INITRD=$(ls /app/initr* | sort | tail -1)

    do_start -kernel $KERNEL \
             -initrd $INITRD \
             -append "root=/dev/sda2 selinux=0 audit=0 rw console=tty0 console=ttyS0" \
             "$@"
}

do_stop() {
    set -x

    do_exec halt
    sleep 2
    pkill qemu

    rm -f /etc/ssh/ssh_config.d/99-vm.conf

    exit 0
}

do_exec() {
    set -x
    ssh -oControlMaster=auto -oControlPersist=5 -oPubkeyAcceptedKeyTypes=ssh-rsa vm "$@"
}

case $1 in
    start)
        shift
        do_start "$@"
        ;;
    start-kernel)
        shift
        do_start_kernel "$@"
        ;;
    stop)
        shift
        do_stop "$@"
        ;;
    exec)
        shift
        do_exec "$@"
        ;;
    *)
        usage
        exit 1
        ;;
esac
