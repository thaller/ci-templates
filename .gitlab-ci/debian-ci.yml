#
# THIS FILE IS GENERATED, DO NOT EDIT

################################################################################
#
# Debian checks
#
################################################################################

#
# Common variable definitions
#
.ci-commons-debian:
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'wget curl'
    FDO_DISTRIBUTION_EXEC: 'sh test/script.sh'
    FDO_DISTRIBUTION_VERSION: 'stretch'
    FDO_EXPIRES_AFTER: '1h'
  needs:
    - sanity check

.ci-commons-debian@x86_64:
  extends: .ci-commons-debian
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-x86_64-$CI_PIPELINE_ID
  needs:
    - bootstrap@x86_64

.ci-commons-debian@aarch64:
  extends: .ci-commons-debian
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID
  needs:
    - bootstrap@x86_64

#
# A few templates to avoid writing the image and stage in each job
#
.debian:ci@container-build@x86_64:
  extends:
    - .fdo.container-build@debian@x86_64
    - .ci-commons-debian@x86_64
  image: $CI_REGISTRY_IMAGE/x86_64/buildah:2020-10-30.1
  stage: debian_container_build


.debian:ci@container-build@aarch64:
  extends:
    - .fdo.container-build@debian@aarch64
    - .ci-commons-debian@aarch64
  image: $CI_REGISTRY_IMAGE/aarch64/buildah:2020-10-30.1
  stage: debian_container_build
  needs:
    - bootstrap@aarch64
    - sanity check


#
# Qemu build
#
.debian:ci@qemu-build@x86_64:
  extends:
    - .fdo.qemu-build@debian@x86_64
    - .ci-commons-debian@x86_64
  image: $CI_REGISTRY_IMAGE/x86_64/qemu-mkosi-base:2020-10-30.1
  stage: debian_container_build
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out
  needs:
    - bootstrap-qemu-mkosi@x86_64
    - sanity check

#
# generic debian checks
#
.debian@check@x86_64:
  extends:
    - .fdo.distribution-image@debian
    - .ci-commons-debian@x86_64
  stage: debian_check
  script:
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - curl --insecure https://gitlab.freedesktop.org
    - wget --no-check-certificate https://gitlab.freedesktop.org
      # make sure our test script has been run
    - if [[ -e /test_file ]] ;
      then
        echo $FDO_DISTRIBUTION_EXEC properly run ;
      else
        exit 1 ;
      fi


.debian@qemu-check@x86_64:
  stage: debian_check
  extends:
    - .fdo.distribution-image@debian
    - .ci-commons-debian@x86_64
  tags:
    - kvm
  script:
    - pushd /app
      # start the VM
    - bash /app/vmctl start
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - /app/vmctl exec curl --insecure https://gitlab.freedesktop.org
    - /app/vmctl exec wget --no-check-certificate https://gitlab.freedesktop.org
      # terminate the VM
    - bash /app/vmctl stop

      # start the VM, with the kernel parameters
    - bash /app/vmctl start-kernel
      # make sure we can still use curl/wget
    - /app/vmctl exec curl --insecure https://gitlab.freedesktop.org
    - /app/vmctl exec wget --no-check-certificate https://gitlab.freedesktop.org
      # terminate the VM
    - bash /app/vmctl stop
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out


#
# straight debian build and test
#
debian:stretch@container-build@x86_64:
  extends: .debian:ci@container-build@x86_64


debian:stretch@check@x86_64:
  extends: .debian@check@x86_64
  needs:
    - debian:stretch@container-build@x86_64
    - sanity check

# Test FDO_BASE_IMAGE. We don't need to do much here, if our
# FDO_DISTRIBUTION_EXEC script can run curl+wget this means we're running on
# the desired base image. That's good enough.
debian:stretch@base-image@x86_64:
  extends: debian:stretch@container-build@x86_64
  stage: debian_check
  variables:
    # We need to duplicate FDO_DISTRIBUTION_TAG here, gitlab doesn't allow nested expansion
    FDO_BASE_IMAGE: registry.freedesktop.org/$CI_PROJECT_PATH/debian/stretch:fdo-ci-x86_64-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_PACKAGES: ''
    FDO_DISTRIBUTION_EXEC: 'test/test-wget-curl.sh'
    FDO_FORCE_REBUILD: 1
    FDO_DISTRIBUTION_TAG: fdo-ci-baseimage-x86_64-$CI_PIPELINE_ID
  needs:
    - debian:stretch@container-build@x86_64
    - sanity check

#
# /cache debian check (in build stage)
#
# Also ensures setting FDO_FORCE_REBUILD will do the correct job
#
debian@cache-container-build@x86_64:
  extends: .debian:ci@container-build@x86_64
  before_script:
      # The template normally symlinks the /cache
      # folder, but we want a fresh new one for the
      # tests.
    - mkdir runner_cache_$CI_PIPELINE_ID
    - uname -a | tee runner_cache_$CI_PIPELINE_ID/foo-$CI_PIPELINE_ID

  artifacts:
    paths:
      - runner_cache_$CI_PIPELINE_ID/*
    expire_in: 1 week

  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-cache-x86_64-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_EXEC: 'bash test/test_cache.sh $CI_PIPELINE_ID'
    FDO_CACHE_DIR: $CI_PROJECT_DIR/runner_cache_$CI_PIPELINE_ID
    FDO_FORCE_REBUILD: 1

#
# /cache debian check (in check stage)
#
debian@cache-check@x86_64:
  stage: debian_check
  image: alpine:latest
  script:
    # in the previous stage (debian@cache-container-build@x86_64),
    # test/test_cache.sh checked for the existance of `/cache/foo-$CI_PIPELINE_ID`
    # and if it found it, it wrote `/cache/bar-$CI_PIPELINE_ID`.
    #
    # So if we have in the artifacts `bar-$CI_PIPELINE_ID`, that means
    # 2 things:
    # - /cache was properly mounted while building the container
    # - the $FDO_CACHE_DIR has been properly written from within the
    #   building container, meaning the /cache folder has been successfully
    #   updated.
    - if [ -e $CI_PROJECT_DIR/runner_cache_$CI_PIPELINE_ID/bar-$CI_PIPELINE_ID ] ;
      then
        echo Successfully read/wrote the cache folder, all good ;
      else
        echo FAILURE while retrieving the previous artifacts ;
        exit 1 ;
      fi
  needs:
    - job: debian@cache-container-build@x86_64
      artifacts: true
    - sanity check


debian:stretch@container-build@aarch64:
  extends: .debian:ci@container-build@aarch64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID


debian:stretch@check@aarch64:
  extends: .debian@check@x86_64
  tags:
    - aarch64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID
  needs:
    - debian:stretch@container-build@aarch64
    - sanity check


debian:stretch@qemu-build@x86_64:
  extends: .debian:ci@qemu-build@x86_64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-qemu-x86_64-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_PACKAGES: 'wget curl'
    QEMU_BASE_IMAGE: $CI_REGISTRY_IMAGE/x86_64/qemu-base:2020-10-30.1


debian:stretch@qemu-check@x86_64:
  extends: .debian@qemu-check@x86_64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-qemu-x86_64-$CI_PIPELINE_ID
  needs:
    - bootstrap-qemu-mkosi@x86_64
    - debian:stretch@qemu-build@x86_64
    - sanity check


#
# make sure we do not rebuild the image if the tag exists (during the check)
#
do not rebuild debian:stretch@container-build@x86_64:
  extends: .debian:ci@container-build@x86_64
  stage: debian_check
  variables:
    FDO_UPSTREAM_REPO: $CI_PROJECT_PATH
    FDO_DISTRIBUTION_PACKAGES: 'this-package-should-not-exist'
  needs:
    - debian:stretch@container-build@x86_64
    - sanity check


#
# check if the labels were correctly applied
#
check labels debian@x86_64:stretch:
  extends:
    - debian:stretch@check@x86_64
  image: $CI_REGISTRY_IMAGE/x86_64/buildah:2020-10-30.1
  script:
    # FDO_DISTRIBUTION_IMAGE still has indirections
    - DISTRO_IMAGE=$(eval echo ${FDO_DISTRIBUTION_IMAGE})

    # retrieve the infos from the registry (once)
    - JSON_IMAGE=$(skopeo inspect docker://$DISTRO_IMAGE)

    # parse all the labels we care about
    - IMAGE_PIPELINE_ID=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.pipeline_id"]')
    - IMAGE_JOB_ID=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.job_id"]')
    - IMAGE_PROJECT=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.project"]')
    - IMAGE_COMMIT=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.commit"]')

    # some debug information
    - echo $JSON_IMAGE
    - echo $IMAGE_PIPELINE_ID $CI_PIPELINE_ID
    - echo $IMAGE_JOB_ID
    - echo $IMAGE_PROJECT $CI_PROJECT_PATH
    - echo $IMAGE_COMMIT $CI_COMMIT_SHA

    # ensure the labels are correct (we are on the same pipeline)
    - '[[ x"$IMAGE_PIPELINE_ID" == x"$CI_PIPELINE_ID" ]]'
    - '[[ x"$IMAGE_JOB_ID" != x"" ]]' # we don't know the job ID, but it must be set
    - '[[ x"$IMAGE_PROJECT" == x"$CI_PROJECT_PATH" ]]'
    - '[[ x"$IMAGE_COMMIT" == x"$CI_COMMIT_SHA" ]]'
  needs:
    - debian:stretch@container-build@x86_64
    - sanity check


#
# make sure we do not rebuild the image if the tag exists in the upstream
# repository (during the check)
# special case where FDO_REPO_SUFFIX == ci_templates_test_upstream
#
pull upstream debian:stretch@container-build@x86_64:
  extends: .debian:ci@container-build@x86_64
  stage: debian_check
  variables:
    FDO_UPSTREAM_REPO: $CI_PROJECT_PATH
    FDO_REPO_SUFFIX: debian/ci_templates_test_upstream
    FDO_DISTRIBUTION_PACKAGES: 'this-package-should-not-exist'
  needs:
    - debian:stretch@container-build@x86_64
    - sanity check

#
# Try our debian scripts with other versions and check
#

debian:sid@container-build@x86_64:
  extends: .debian:ci@container-build@x86_64
  variables:
    FDO_DISTRIBUTION_VERSION: 'sid'

debian:sid@check@x86_64:
  extends: .debian@check@x86_64
  variables:
    FDO_DISTRIBUTION_VERSION: 'sid'
  needs:
    - debian:sid@container-build@x86_64
    - sanity check

debian:buster@container-build@x86_64:
  extends: .debian:ci@container-build@x86_64
  variables:
    FDO_DISTRIBUTION_VERSION: 'buster'

debian:buster@check@x86_64:
  extends: .debian@check@x86_64
  variables:
    FDO_DISTRIBUTION_VERSION: 'buster'
  needs:
    - debian:buster@container-build@x86_64
    - sanity check
