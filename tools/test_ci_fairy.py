#!/usr/bin/env python3

from click.testing import CliRunner
from unittest.mock import patch, MagicMock
import botocore
import git
import json
import pytest
from pathlib import Path

import ci_fairy

GITLAB_TEST_URL = 'https://test.gitlab.url'
GITLAB_TEST_PROJECT_ID = '11'
GITLAB_TEST_PROJECT_PATH = 'project12/path34'

MINIO_TEST_SERVER = 'min.io.url:9000'
MINIO_TEST_URL = 'http://min.io.url:9000'

# A note on @patch('ci_fairy.Gitlab')
# because we use from gitlab import Gitlab, the actual instance sits in
# ci_fairy.Gitlab and we need to patch that instance.


@pytest.fixture
def gitlab_url():
    return GITLAB_TEST_URL


@pytest.fixture
def gitlab_project_path():
    return GITLAB_TEST_PROJECT_PATH


@pytest.fixture
def gitlab_default_env():
    return {
        'CI': '1',
        'CI_SERVER_URL': GITLAB_TEST_URL,
        'CI_PROJECT_PATH': GITLAB_TEST_PROJECT_PATH,
        'CI_PROJECT_ID': GITLAB_TEST_PROJECT_ID,
    }


def mock_gitlab(gitlab):
    repos = []
    for i in range(3):
        repo = MagicMock()
        repo.name = f'repository/{i}'
        repos.append(repo)

        tags = []
        for t in range(5):
            tag = MagicMock()
            tag.name = f'tag-{t}'
            tags.append(tag)

        repo.tags = MagicMock()
        repo.tags.list = MagicMock(return_value=tags)

    project = MagicMock()
    project.name = GITLAB_TEST_PROJECT_PATH
    project.id = GITLAB_TEST_PROJECT_ID
    project.repositories.list = MagicMock(return_value=repos)

    mrs = []
    for i in range(4):
        mr = MagicMock()
        mr.state = 'opened' if i < 2 else 'merged'
        mr.id = i
        mr.sha = f'dead{i:08x}'
        mr.allow_collaboration = (i % 2) == 0
        mrs.append(mr)

    def mrget(mid):
        return [m for m in mrs if m.id == int(mid)][0]

    def mrlist(state=None, per_page=None, order_by='created_at'):
        # order_by is ignored for our tests
        return [m for m in mrs if state is None or m.state == state]

    project.mergerequests.list = MagicMock(side_effect=mrlist)
    project.mergerequests.get = MagicMock(side_effect=mrget)

    ctx = gitlab(GITLAB_TEST_URL)
    ctx.projects.list = MagicMock(return_value=[project])
    # This always returns our project, even where the ID doesn't match. Good
    # enough for tests but may cause false positives.
    ctx.projects.get = MagicMock(return_value=project)

    return ctx, project, repos


@patch('ci_fairy.Gitlab')
def test_project_lookup(gitlab, caplog, gitlab_default_env, monkeypatch):
    # instantiate here so ci-fairy will instantiate the same objects
    ctx = gitlab(GITLAB_TEST_URL)

    monkeypatch.setenv('CI_PROJECT_ID', '3')

    class Project():
        def __init__(self, name, id):
            self.name = name
            self.id = id

    projects = []
    for idx, name in enumerate(['foo', 'user/foo', 'foobar', 'foo/foo']):
        projects.append(Project(name, idx))

    def pget(pid):
        return [p for p in projects if p.id == int(pid)][0]

    ctx.projects.get = MagicMock(side_effect=pget)

    def plist(search, search_namespaces=True):
        return [p for p in projects if search in p.name]

    ctx.projects.list = MagicMock(side_effect=plist)

    # default is CI_PROJECT_ID
    p = ci_fairy.gitlab_project(ctx)
    assert p is not None
    assert p.name == 'foo/foo'
    assert p.id == 3

    # exact match
    p = ci_fairy.gitlab_project(ctx, 'user/foo')
    assert p is not None
    assert p.name == 'user/foo'
    assert p.id == 1

    # require exact match over substring match
    p = ci_fairy.gitlab_project(ctx, 'foo')
    assert p is not None
    assert p.name == 'foo'
    assert p.id == 0

    # exact match
    p = ci_fairy.gitlab_project(ctx, 'foobar')
    assert p is not None
    assert p.name == 'foobar'
    assert p.id == 2

    # exact match
    p = ci_fairy.gitlab_project(ctx, 'foo/foo')
    assert p is not None
    assert p.name == 'foo/foo'
    assert p.id == 3


def test_missing_url(caplog, gitlab_default_env):
    args = ['delete-image', '--all', '--dry-run']  # need one subcommand

    env = gitlab_default_env
    env['CI_SERVER_URL'] = ''

    runner = CliRunner(env=env)
    # Must not be run within our git repo, otherwise it'll default to fdo
    with runner.isolated_filesystem():
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert 'Missing gitlab URL' in [r.msg for r in caplog.records]


@patch('ci_fairy.Gitlab')
def test_url_extractor(gitlab, caplog, gitlab_default_env):
    args = ['delete-image', '--all', '--dry-run']  # need one subcommand

    env = gitlab_default_env
    env['CI_SERVER_URL'] = ''
    env['CI_JOB_TOKEN'] = ''

    urls = [
        'https://foo.bar/repo.git',
        'http://foo.bar/repo.git',
        'http://user@foo.bar/path/repo.git',
        'user@foo.bar:path/repo.git',
        'ssh://user@foo.bar:path/repo.git',
        'git+ssh://git@foo.bar:path/repo.git',
        'https://foo.bar:path/repo.git',
    ]
    for url in urls:
        runner = CliRunner(env=env)
        # Must not be run within our git repo, otherwise it'll default to fdo
        with runner.isolated_filesystem():
            repo = git.Repo.init()
            repo.create_remote('origin', url)
            runner.invoke(ci_fairy.ci_fairy, args)
            gitlab.assert_called_with('https://foo.bar')


@patch('ci_fairy.Gitlab')
def test_no_auth(gitlab, caplog, gitlab_default_env):
    args = ['delete-image', '--all', '--dry-run']  # need one subcommand

    env = gitlab_default_env
    env['CI_JOB_TOKEN'] = ''

    runner = CliRunner(env=env)
    runner.invoke(ci_fairy.ci_fairy, args)
    gitlab.assert_called_with(GITLAB_TEST_URL)


@patch('ci_fairy.Gitlab')
def test_job_token_auth(gitlab, caplog, gitlab_default_env):
    token = 'tokenval'
    args = ['delete-image', '--all', '--dry-run']  # need one subcommand
    env = gitlab_default_env.copy()
    env['CI_JOB_TOKEN'] = token
    runner = CliRunner(env=env)
    runner.invoke(ci_fairy.ci_fairy, args)
    gitlab.assert_called_with(GITLAB_TEST_URL, job_token=token)


@patch('ci_fairy.Gitlab')
def test_private_token_auth(gitlab, caplog, gitlab_default_env):
    token = 'tokenval'
    authfile = 'afile'
    args = ['--authfile', authfile, 'delete-image', '--all', '--dry-run']  # need one subcommand
    env = gitlab_default_env.copy()
    # --authfile overrides CI_JOB_TOKEN
    env['CI_JOB_TOKEN'] = '123245'
    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        with open(authfile, 'w') as fd:
            fd.write(token)
        runner.invoke(ci_fairy.ci_fairy, args)
        gitlab.assert_called_with(GITLAB_TEST_URL, private_token=token)


@patch('ci_fairy.Gitlab')
def test_delete_image_missing_arg(gitlab, caplog, gitlab_default_env):
    args = ['delete-image']
    runner = CliRunner(env=gitlab_default_env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 1
    assert 'One of --tag, --exclude-tag, or --all is required.' in [r.msg for r in caplog.records]


@patch('ci_fairy.Gitlab')
def test_delete_image_invalid_project(gitlab, caplog, gitlab_default_env):
    # instantiate here so ci-fairy will instantiate the same objects
    ctx = gitlab(GITLAB_TEST_URL)

    p1 = MagicMock()
    p1.name = 'foo'
    p1.id = 0
    p2 = MagicMock()
    p2.name = 'foo'
    p2.id = 1

    # empty list or more than one of projects triggers an error
    for rv in [[], [p1, p2]]:
        ctx.projects.list = MagicMock(return_value=rv)
        args = ['delete-image', '--all', '--project', 'foo']
        runner = CliRunner(env=gitlab_default_env)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert 'Unable to find or identify project' in [r.msg for r in caplog.records]


@pytest.mark.parametrize('reponame', [None, 'repository-3'])
@pytest.mark.parametrize('dry_run', [True, False])
@patch('ci_fairy.Gitlab')
def test_delete_image_all(gitlab, caplog, gitlab_default_env, reponame, dry_run):
    gitlab, project, repos = mock_gitlab(gitlab)

    args = ['delete-image', '--all']
    if reponame:
        args += ['--repository', reponame]
    if dry_run:
        args += ['--dry-run']
    runner = CliRunner(env=gitlab_default_env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0

    for r in repos:
        for t in r.tags.list():
            if dry_run:
                t.delete.assert_not_called()
            elif reponame is None or reponame == r.name:
                t.delete.assert_called()
            else:
                t.delete.assert_not_called()


@pytest.mark.parametrize('reponame', [None, 'repository-2'])
@patch('ci_fairy.Gitlab')
def test_delete_image_exclude_tag(gitlab, caplog, gitlab_default_env, reponame):
    gitlab, project, repos = mock_gitlab(gitlab)

    TAGNAME = 'tag-2'

    args = ['delete-image', '--exclude-tag', TAGNAME]
    if reponame:
        args += ['--repository', reponame]
    runner = CliRunner(env=gitlab_default_env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0

    for r in repos:
        for t in r.tags.list():
            if t.name == TAGNAME or (reponame and reponame != r.name):
                t.delete.assert_not_called()
            else:
                t.delete.assert_called()


@pytest.mark.parametrize('reponame', [None, 'repository-1'])
@patch('ci_fairy.Gitlab')
def test_delete_image_tag(gitlab, caplog, gitlab_default_env, reponame):
    gitlab, project, repos = mock_gitlab(gitlab)

    TAGNAME = 'tag-1'

    args = ['delete-image', '--tag', TAGNAME]
    if reponame:
        args += ['--repository', reponame]
    runner = CliRunner(env=gitlab_default_env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0

    for r in repos:
        for t in r.tags.list():
            if (reponame is None or reponame == r.name) and t.name == TAGNAME:
                t.delete.assert_called()
            else:
                t.delete.assert_not_called()


@pytest.mark.parametrize('reponame', [None, 'repository-1'])
@patch('ci_fairy.Gitlab')
def test_delete_image_tag_fnmatch(gitlab, caplog, gitlab_default_env, reponame):
    gitlab, project, repos = mock_gitlab(gitlab)

    TAGNAME = 'tag-*'

    args = ['delete-image', '--tag', TAGNAME]
    if reponame:
        args += ['--repository', reponame]
    runner = CliRunner(env=gitlab_default_env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0

    for r in repos:
        for t in r.tags.list():
            if (reponame is None or reponame == r.name):
                t.delete.assert_called()
            else:
                t.delete.assert_not_called()


@pytest.mark.parametrize('yamlfile', [None, 'custom.yml'])
@patch('ci_fairy.Gitlab')
def test_lint(gitlab, caplog, gitlab_default_env, yamlfile):
    gitlab, *_ = mock_gitlab(gitlab)

    gitlab.lint = MagicMock(return_value=(True, []))

    args = ['lint']
    if yamlfile is not None:
        args += ['--filename', yamlfile]
    else:
        yamlfile = '.gitlab-ci.yml'

    runner = CliRunner(env=gitlab_default_env)
    with runner.isolated_filesystem():
        with open(yamlfile, 'w') as fd:
            fd.write('test: ci')
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        gitlab.lint.assert_called_with('test: ci')


DEFAULT_YAML = '''
numbers:
    one: 1
    two: 2
    three: 3

fruit:
  apple:
    color: green
    health: 78
  banana:
    color: yellow
    health: 52
  orange:
    color: orange
    health: 65
'''


def test_template_simple():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open('test.yml', 'w') as fd:
            fd.write(DEFAULT_YAML)
        with open('test.tmpl', 'w') as fd:
            fd.write('three is {{numbers.three}}')

        args = ['generate-template', '--config', 'test.yml', 'test.tmpl']

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert result.stdout == 'three is 3'


def test_template_loop():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open('test.yml', 'w') as fd:
            fd.write(DEFAULT_YAML)
        with open('test.tmpl', 'w') as fd:
            fd.write('{% for f in fruit %}{{f}}{% endfor %}')

        args = ['generate-template', '--config', 'test.yml', 'test.tmpl']

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert result.stdout == 'applebananaorange'


def test_template_defaults():
    runner = CliRunner()
    with runner.isolated_filesystem():
        cidir = Path('.gitlab-ci')
        cidir.mkdir()
        config = cidir / 'config.yml'
        template = cidir / 'ci.template'
        with open(config, 'w') as fd:
            fd.write(DEFAULT_YAML)
        with open(template, 'w') as fd:
            fd.write('three is {{numbers.three}}')

        args = ['generate-template']

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0

        outfile = Path('.gitlab-ci.yml')
        assert outfile.exists()
        assert open(outfile).read() == 'three is 3'


@pytest.mark.parametrize('rootnode', ['fruit', '/fruit'])
def test_template_root(rootnode):
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open('test.yml', 'w') as fd:
            fd.write(DEFAULT_YAML)
        with open('test.tmpl', 'w') as fd:
            fd.write('health: {{banana.health}}')

        args = ['generate-template', '--config', 'test.yml',
                '--root', rootnode, 'test.tmpl']

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert result.stdout == 'health: 52'


def test_template_root_path():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open('test.yml', 'w') as fd:
            fd.write(DEFAULT_YAML)
        with open('test.tmpl', 'w') as fd:
            fd.write('health: {{health}}')

        args = ['generate-template', '--config', 'test.yml',
                '--root', '/fruit/orange', 'test.tmpl']

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert result.stdout == 'health: 65'


def test_commits_needs_git_repo(caplog):
    runner = CliRunner()
    with runner.isolated_filesystem():
        args = ['check-commits']
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert 'This must be run from within the git repository' in ''.join(caplog.messages)


def test_commits_noop(gitlab_default_env):
    env = gitlab_default_env
    env['CI'] = ''
    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        args = ['check-commits']

        repo = git.Repo.init()
        config = repo.config_writer()
        config.set_value('user', 'email', 'test@example.com')
        config.set_value('user', 'name', 'test user')
        config.release()
        with open('test', 'w') as fd:
            fd.write('foo')
            repo.index.add(['test'])
            repo.index.commit('initial commit')

        # nothing to compare here, we're on master
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0


def test_commits_fixup_squash(caplog, gitlab_default_env):
    env = gitlab_default_env
    env['CI'] = ''

    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        args = ['check-commits']

        repo = git.Repo.init()
        config = repo.config_writer()
        config.set_value('user', 'email', 'test@example.com')
        config.set_value('user', 'name', 'test user')
        config.release()
        with open('test', 'w') as fd:
            fd.write('foo')
            repo.index.add(['test'])
            repo.index.commit('initial commit')

        b = repo.create_head('mybranch')
        repo.head.reference = b
        with open('test', 'w') as fd:
            fd.write('bar')
            repo.index.add(['test'])
            repo.index.commit('fixup! blah')

            # whack on another commit to verify we're testing the range, not
            # just HEAD
            fd.write('baz')
            repo.index.add(['test'])
            repo.index.commit('normal commit')

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert 'Leftover "fixup!"' in ''.join(caplog.messages)

        b = repo.create_head('mybranch2', 'master')
        repo.head.reference = b
        repo.head.reset(index=True, working_tree=True)

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0

        with open('test', 'w') as fd:
            fd.write('bar')
            repo.index.add(['test'])
            repo.index.commit('squash! blah')

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert 'Leftover "fixup!"' in ''.join(caplog.messages)


def test_commits_sob(caplog, gitlab_default_env):
    env = gitlab_default_env
    env['CI'] = ''

    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        repo = git.Repo.init()
        config = repo.config_writer()
        config.set_value('user', 'email', 'test@example.com')
        config.set_value('user', 'name', 'test user')
        config.release()

        with open('test', 'w') as fd:
            fd.write('foo')
            repo.index.add(['test'])
            repo.index.commit('initial commit')

        b = repo.create_head('mybranch')
        repo.head.reference = b
        with open('test', 'w') as fd:
            fd.write('bar')
            repo.index.add(['test'])
            repo.index.commit('Lacking a s-o-b')

        args = ['check-commits']
        result = runner.invoke(ci_fairy.ci_fairy, args + ['--no-signed-off-by'])
        assert result.exit_code == 0

        result = runner.invoke(ci_fairy.ci_fairy, args + ['--signed-off-by'])
        assert result.exit_code == 1
        assert 'Missing "Signed-off-by: author information"' in ''.join(caplog.messages)

        b = repo.create_head('mybranch2', 'master')
        repo.head.reference = b
        repo.head.reset(index=True, working_tree=True)
        with open('test', 'w') as fd:
            fd.write('bar')
            repo.index.add(['test'])
            repo.index.commit('Commit\n\nSigned-off-by: first last <email>')

        result = runner.invoke(ci_fairy.ci_fairy, args + ['--no-signed-off-by'])
        assert result.exit_code == 1
        assert 'Do not use Signed-off-by in commits' in ''.join(caplog.messages)

        result = runner.invoke(ci_fairy.ci_fairy, args + ['--signed-off-by'])
        assert result.exit_code == 0

        # Signed-off-by: must be at the beginning of the line
        b = repo.create_head('mybranch3', 'master')
        repo.head.reference = b
        repo.head.reset(index=True, working_tree=True)
        with open('test', 'w') as fd:
            fd.write('bar')
            repo.index.add(['test'])
            repo.index.commit('Commit\n\n___Signed-off-by: first last <email>')

        result = runner.invoke(ci_fairy.ci_fairy, args + ['--no-signed-off-by'])
        assert result.exit_code == 0

        result = runner.invoke(ci_fairy.ci_fairy, args + ['--signed-off-by'])
        assert result.exit_code == 1
        assert 'Missing "Signed-off-by: author information"' in ''.join(caplog.messages)


def test_commits_msgformat(caplog, gitlab_default_env):
    env = gitlab_default_env
    env['CI'] = ''

    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        args = ['check-commits']

        repo = git.Repo.init()
        config = repo.config_writer()
        config.set_value('user', 'email', 'test@example.com')
        config.set_value('user', 'name', 'test user')
        config.release()

        with open('test', 'w') as fd:
            fd.write('foo')
            repo.index.add(['test'])
            repo.index.commit('initial commit')

        b = repo.create_head('mybranch')
        repo.head.reference = b
        with open('test', 'w') as fd:
            fd.write('bar')
            repo.index.add(['test'])
            # second line must be empty
            repo.index.commit('three\nline\ncommit')

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert 'Second line in commit message must be empty' in ''.join(caplog.messages)

        b = repo.create_head('mybranch2', 'master')
        repo.head.reference = b
        repo.head.reset(index=True, working_tree=True)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        with open('test', 'w') as fd:
            fd.write('bar')
            repo.index.add(['test'])
            repo.index.commit('too long a line' * 10)

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert 'Commit message subject must not exceed' in ''.join(caplog.messages)

        result = runner.invoke(ci_fairy.ci_fairy, args + ['--textwidth=1000'])
        assert result.exit_code == 0

        b = repo.create_head('mybranch3', 'master')
        repo.head.reference = b
        repo.head.reset(index=True, working_tree=True)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        with open('test', 'w') as fd:
            fd.write('bar')
            repo.index.add(['test'])
            # anything other than the subject can exceed the textwidth
            repo.index.commit('short-enough subject\n\ntoo long a line' * 10)

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0


def test_commits_gitlabemail(caplog, gitlab_default_env):
    env = gitlab_default_env
    env['CI'] = ''

    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        args = ['check-commits']

        repo = git.Repo.init()
        config = repo.config_writer()
        config.set_value('user', 'email', 'testuser@users.noreply.gitlab.freedesktop.org')
        config.set_value('user', 'name', 'test user')
        config.release()

        with open('test', 'w') as fd:
            fd.write('foo')
            repo.index.add(['test'])
            repo.index.commit('initial commit')

        b = repo.create_head('mybranch')
        repo.head.reference = b
        with open('test', 'w') as fd:
            fd.write('bar')
            repo.index.add(['test'])
            repo.index.commit('second commit')

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert 'git author email invalid' in ''.join(caplog.messages)


@patch('ci_fairy.Gitlab')
def test_merge_request_missing_arg(gitlab, caplog, gitlab_default_env):
    args = ['check-merge-request']

    runner = CliRunner(env=gitlab_default_env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 1
    assert 'At least one check must be specified' in caplog.text


@patch('ci_fairy.Gitlab')
def test_merge_request_detached_pipeline(gitlab, caplog, gitlab_default_env):
    args = ['check-merge-request', '--require-allow-collaboration']

    env = gitlab_default_env
    env['CI_MERGE_REQUEST_PROJECT_ID'] = GITLAB_TEST_PROJECT_ID
    env['CI_MERGE_REQUEST_IID'] = '1'

    # mock_gitlab sets allow_collaboration off for every second MR
    gitlab, project, _ = mock_gitlab(gitlab)
    runner = CliRunner(env=env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 1
    assert 'This merge request does not allow edits from maintainers' in caplog.text

    env['CI_MERGE_REQUEST_IID'] = '0'
    gitlab, project, _ = mock_gitlab(gitlab)
    runner = CliRunner(env=env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0


@patch('ci_fairy.Gitlab')
def test_merge_request_fdo_upstream_repo(gitlab, caplog, gitlab_default_env):
    args = ['-vv', 'check-merge-request', '--require-allow-collaboration']

    env = gitlab_default_env
    env['FDO_UPSTREAM_REPO'] = GITLAB_TEST_PROJECT_ID
    env['CI_PROJECT_ID'] = '9999'
    env['CI_COMMIT_SHA'] = 'dead00000001'

    # mock_gitlab sets allow_collaboration off for every second MR
    gitlab, project, _ = mock_gitlab(gitlab)
    runner = CliRunner(env=env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 1
    assert 'This merge request does not allow edits from maintainers' in caplog.text

    env['CI_COMMIT_SHA'] = 'dead00000000'
    gitlab, project, _ = mock_gitlab(gitlab)
    runner = CliRunner(env=env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0


@patch('ci_fairy.Gitlab')
def test_merge_request_already_merged(gitlab, caplog, gitlab_default_env):
    args = ['-vv', 'check-merge-request', '--require-allow-collaboration']

    env = gitlab_default_env
    env['FDO_UPSTREAM_REPO'] = GITLAB_TEST_PROJECT_ID
    env['CI_PROJECT_ID'] = '9999'
    env['CI_COMMIT_SHA'] = 'dead00000002'  # MRs 2 and 3 are 'merged'

    # mock_gitlab sets allow_collaboration off for every second MR
    gitlab, project, _ = mock_gitlab(gitlab)
    runner = CliRunner(env=env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0
    assert 'Merge request !2 is already merged, skipping checks' in caplog.text

    env['CI_COMMIT_SHA'] = 'dead00000003'
    gitlab, project, _ = mock_gitlab(gitlab)
    runner = CliRunner(env=env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0
    assert 'Merge request !3 is already merged, skipping checks' in caplog.text


def mock_s3_session(session):
    client = MagicMock(name='client')

    ctx = session()
    ctx.client = client

    return ctx


@patch('json.dump')
@patch('ci_fairy.boto3.Session')
def test_minio_login(session, json_dump, caplog):
    args = ['-vv', 'minio', 'login', '--endpoint-url', MINIO_TEST_URL, '1234']

    # check that the endpoint we provide is actually used by the botocore client
    ctx = mock_s3_session(session)
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(ci_fairy.ci_fairy, args, catch_exceptions=False)

        assert result.exit_code == 0

        # python 3.8 allows to have `mock.call_args.kwargs`, but python 3.7
        # doesn't. So use the 3.7 version (tuple access) until 3.8 gets
        # more widespread.
        assert ctx.client.call_args[1]['endpoint_url'] == MINIO_TEST_URL

    # ensure that if the caller forgets about the token ('1234' above), we fail
    args = ['-vv', 'minio', 'login', '--endpoint-url', MINIO_TEST_URL]
    with runner.isolated_filesystem():
        result = runner.invoke(ci_fairy.ci_fairy, args)

        assert result.exit_code == 2


def mock_minio(minio):
    error_response = {'Error': {'Code': 404, 'Message': 'Not Found'}}
    _404 = botocore.exceptions.ClientError(error_response, 'HeadObject')
    buckets = []
    for i in range(3):
        bucket = MagicMock()
        bucket.name = f'bucket{i}'
        buckets.append(bucket)

        files = []
        f = MagicMock()
        f.key = 'root_file.txt'
        files.append(f)
        for _dir in range(5):
            for _f in range(4):
                f = MagicMock()
                f.key = f'dir-{_dir}/file-{_f}'
                files.append(f)

        def upload_file(src, dst):
            f = MagicMock()
            f.key = dst
            files.append(f)

        def download_file(src, dst):
            if src not in [f.key for f in files]:
                raise _404
            with open(dst, 'w') as f:
                f.write('Hello World!')

        def list_objects():
            return files

        bucket.objects.all.side_effect = list_objects
        bucket.upload_file.side_effect = upload_file
        bucket.download_file.side_effect = download_file

    ctx = minio(endpoint_url=MINIO_TEST_URL)
    ctx.buckets.all = MagicMock(return_value=buckets)

    def bucket_side_effect(arg):
        for b in buckets:
            if b.name == arg:
                return b
        bucket = MagicMock()
        bucket.name = arg
        bucket.objects.all = MagicMock(return_value=[])

        bucket.upload_file.side_effect = _404
        bucket.download_file.side_effect = _404
        return bucket

    ctx.Bucket.side_effect = bucket_side_effect

    return ctx, buckets


def write_minio_credentials():
    with open('.minio_credentials', 'w') as f:
        json.dump({
            MINIO_TEST_SERVER: {
                'endpoint_url': MINIO_TEST_URL,
                'AccessKeyId': '1234',
                'SecretAccessKey': '5678',
                'SessionToken': '9101112',
            }
        }, f)


@patch('ci_fairy.boto3.resource')
@pytest.mark.parametrize("input_path,result_files", [
    (None, ['hello.txt', '.minio_credentials']),
    ('.', ['hello.txt', '.minio_credentials']),
    ('minio:', None),
    ('minio:/', None),
    ('minio://', None),
    (f'minio://{MINIO_TEST_SERVER}', [f'bucket{i}' for i in range(3)]),
    (f'minio://{MINIO_TEST_SERVER}/', [f'bucket{i}' for i in range(3)]),
    ('minio://WRONG_MINIO_TEST_SERVER', None),
    ('minio://WRONG_MINIO_TEST_SERVER/bucket/path', None),
    (f'minio://{MINIO_TEST_SERVER}/bucket1', ['root_file.txt'] + [f'dir-{i}' for i in range(5)]),
    (f'minio://{MINIO_TEST_SERVER}/bucket0/', ['root_file.txt'] + [f'dir-{i}' for i in range(5)]),
    (f'minio://{MINIO_TEST_SERVER}/non_existant_bucket', None),
    (f'minio://{MINIO_TEST_SERVER}/bucket0/non_existent_dir_or_file', None),
    (f'minio://{MINIO_TEST_SERVER}/bucket2/dir-2', [f'file-{i}' for i in range(4)]),
    (f'minio://{MINIO_TEST_SERVER}/bucket0/dir-1/', [f'file-{i}' for i in range(4)]),
    (f'minio://{MINIO_TEST_SERVER}/bucket2/dir-0/file-3', ['file-3']),
])
def test_minio_ls(minio, input_path, result_files, caplog):
    runner = CliRunner()

    args = ['minio', 'ls']

    if input_path is not None:
        args.append(input_path)

    ctx, buckets = mock_minio(minio)

    with runner.isolated_filesystem():
        write_minio_credentials()

        with open('hello.txt', 'w') as f:
            f.write('Hello World!')

        result = runner.invoke(ci_fairy.ci_fairy, args, catch_exceptions=False)

        if result_files is None:
            assert result.exit_code == 2
            prefix = 'minio://'
            error_msg = result.output.strip().split('\n')[-1]

            if input_path.startswith(prefix):
                input_path = input_path[len(prefix):]

            if input_path.startswith('WRONG_MINIO_TEST_SERVER'):
                input_path = 'WRONG_MINIO_TEST_SERVER'
            elif input_path.startswith(MINIO_TEST_SERVER):
                input_path = MINIO_TEST_SERVER

            assert input_path in error_msg

        else:
            assert result.exit_code == 0

            output = result.output.strip('\n').split('\n')
            assert set(result_files) == set(output)


@patch('ci_fairy.boto3.resource')
def test_minio_ls_no_creds(minio, caplog):
    runner = CliRunner()

    args = ['minio', 'ls']

    ctx, buckets = mock_minio(minio)

    with runner.isolated_filesystem():
        with open('hello.txt', 'w') as f:
            f.write('Hello World!')

        result = runner.invoke(ci_fairy.ci_fairy, args, catch_exceptions=False)

    assert result.exit_code == 2


@patch('ci_fairy.boto3.resource')
@pytest.mark.parametrize("input_args, expected_error", [
    #######################################################
    # working cases
    #######################################################

    # copy a local file
    (['hello.txt', 'an_other_local_file'], None),

    # copy a local file to a dir
    (['hello.txt', 'emptydir/'], None),

    # copy a local file to a dir
    (['hello.txt', 'emptydir/an_other_local_file'], None),

    # download an existing distant file
    ([f'minio://{MINIO_TEST_SERVER}/bucket0/dir-1/file-2', 'result'], None),

    # download an existing distant file on an existing dir
    ([f'minio://{MINIO_TEST_SERVER}/bucket0/dir-1/file-2', 'emptydir/'], None),

    # download an existing distant file on an existing dir
    ([f'minio://{MINIO_TEST_SERVER}/bucket0/dir-1/file-2', 'emptydir/result'], None),

    # download an existing distant file on an existing dir
    ([f'minio://{MINIO_TEST_SERVER}/bucket1/dir-2/file-3', 'emptydir//result'], None),

    # download an existing distant file on an existing dir
    ([f'minio://{MINIO_TEST_SERVER}/bucket2/dir-0/file-2', 'emptydir///result'], None),

    # upload a local file on an existing bucket
    (['hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket0/'], None),

    # upload a local file on an existing bucket
    (['hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket0/hello2.txt'], None),

    # upload a local file on a new dir on an existing bucket
    (['hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket1/new_dir/'], None),

    # upload a local file on a new dir on an existing bucket
    (['hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket0/new_dir/hello2.txt'], None),

    # upload a local file on an existing dir on an existing bucket
    (['hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket0/dir-1/'], None),

    # upload a local file on an existing dir on an existing bucket
    (['hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket0/dir-1/hello2.txt'], None),

    # upload a local file on an existing dir on an existing bucket
    (['emptydir/../hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket2/dir-3/'], None),

    # upload a local file on an existing dir on an existing bucket
    (['hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket0//dir-1/hello2.txt'], None),

    #######################################################
    # failures
    #######################################################

    # click error: not enough arguments
    ([], 'Error: Missing argument'),

    # click error: not enough arguments
    (['local_file'], 'Error: Missing argument'),

    # click error: too many arguments
    (['local_file_1', 'local_file_2', 'local_file_3'], 'Error: Got unexpected extra argument (local_file_3)'),

    # source file does not exist
    (['local_file', 'an_other_local_file'], "Error: source file 'local_file' does not exist"),

    # source file does not exist
    ([f'minio://{MINIO_TEST_SERVER}/bucket_that_does_not_exist/file', 'an_other_local_file'], "404"),

    # source file does not exist
    ([f'minio://{MINIO_TEST_SERVER}/bucket1/file', 'an_other_local_file'], "404"),

    # source file is a local dir
    (['.', 'local_file'], 'Error: cannot do recursive cp'),

    # source file is a remote dir
    ([f'minio://{MINIO_TEST_SERVER}/bucket0/', 'local_file'], 'Error: cannot do recursive cp'),

    # source file is a remote dir
    ([f'minio://{MINIO_TEST_SERVER}/bucket1/dir-0', 'local_file'], '404'),

    # source file is a remote dir
    ([f'minio://{MINIO_TEST_SERVER}/bucket1/dir-1/', 'local_file'], 'Error: cannot do recursive cp'),

    # source file is a local empty dir
    (['emptydir', 'local_file'], 'Error: cannot do recursive cp'),

    # source and destination file are remote
    ([f'minio://{MINIO_TEST_SERVER}/bucket1/dir-0/file-3', f'minio://{MINIO_TEST_SERVER}/bucket2/an_other_local_file'], 'Error: at least one argument must be a local path'),

    # destination is invalid (no host)
    (['hello.txt', 'minio://'], 'Error: "missing host information in \'minio://\'"'),

    # destination is invalid (no bucket)
    (['hello.txt', f'minio://{MINIO_TEST_SERVER}/'], 'Error: No destination bucket provided'),

    # destination is invalid (wrong host)
    (['hello.txt', 'minio://WRONG_MINIO_TEST_SERVER/'], 'Error: "host \'WRONG_MINIO_TEST_SERVER\' not found in credentials'),

    # destination is invalid (no bucket)
    (['hello.txt', f'minio://{MINIO_TEST_SERVER}//dir-0/'], '404'),

    # destination is invalid (wrong bucket)
    (['hello.txt', f'minio://{MINIO_TEST_SERVER}/this_bucket_doesnt_exist/dir-0/'], '404'),

    # destination is invalid (new dir)
    ([f'minio://{MINIO_TEST_SERVER}/bucket1/dir-0/file-1', 'new_dir/'], "Error: directory 'new_dir' does not exist"),
])
def test_minio_cp(minio, input_args, expected_error, caplog):
    runner = CliRunner()

    default_args = ['minio', 'cp']

    args = default_args + input_args

    ctx, buckets = mock_minio(minio)

    with runner.isolated_filesystem():
        write_minio_credentials()

        with open('hello.txt', 'w') as f:
            f.write('Hello World!')

        Path('emptydir').mkdir()

        result = runner.invoke(ci_fairy.ci_fairy, args, catch_exceptions=False)

        if expected_error is None:

            assert result.exit_code == 0

            check_file = input_args[1]
            if check_file.endswith('/'):
                check_file += Path(input_args[0]).name

            args = ['minio', 'ls', check_file]
            check_result = runner.invoke(ci_fairy.ci_fairy, args, catch_exceptions=False)

            assert check_result.exit_code == 0

            if check_file.startswith('minio://'):
                check_file = check_file[len('minio://'):]

            path = Path(check_file)

            assert path.name in check_result.output

        else:
            assert result.exit_code == 2
            error_msg = result.output.strip().split('\n')[-1]

            assert expected_error in error_msg
